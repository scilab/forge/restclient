//<-- NO CHECK REF -->


//REST POST, custom Headers and POST arguments provided as parameters
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
options = struct("argsFromFile",%f,"argsSrc","myarg=myvalue","authUser","","authPass","","resultDest",TMPDIR);
result = restPOST('http://greensuisse.zzl.org/product/dump/dump.php',options,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
resultContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(resultContents,"myvalue") == []);
assert_checkfalse(grep(resultContents,"myheaderval") == []);


//REST POST, custom Headers and POST arguments provided as parameters
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
options = struct("argsFromFile",%f,"argsSrc","myarg=myvalue","authUser","","authPass","","resultDest",TMPDIR);
result = restPOST('http://www.newburghschools.org/testfolder/dump.php?urlarg=urlvalue',options,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
resultContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(resultContents,"urlvalue") == []);
assert_checkfalse(grep(resultContents,"myvalue") == []);
assert_checkfalse(grep(resultContents,"myheaderval") == []);


//REST POST, custom Headers and POST arguments provided as parameters
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restPOST("http://httpbin.org/post",%f,"myarg=myvalue","","",TMPDIR,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
resultContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(resultContents,"myvalue") == []);
assert_checkfalse(grep(resultContents,"myheaderval") == []);


//REST POST, using Options struct, custom Headers and POST arguments provided through a file
srcFile = TMPDIR + "/restTemp";
f1 = mopen(srcFile,"w");
mputl("myarg=myvalue",f1);
mclose(f1);
inf = fileinfo(srcFile);
assert_checkfalse(inf(1) == 0);
options = struct("argsFromFile",%t,"argsSrc",srcFile,"authUser","","authPass","","resultDest",TMPDIR);
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restPOST("http://httpbin.org/post",options,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"myvalue") == []);
assert_checkfalse(grep(fileContents,"myheaderval2") == []);

//REST POST, with http://posttestserver.com/post.php, custom Headers and POSTS arguments provided through a file
srcFile = TMPDIR + "/restTemp";
f1 = mopen(srcFile,"w");
mputl("myarg=myvalue",f1);
mclose(f1);
inf = fileinfo(srcFile);
assert_checkfalse(inf(1) == 0);
options = struct("argsFromFile",%t,"argsSrc",srcFile,"authUser","","authPass","","resultDest",TMPDIR);
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restPOST('http://posttestserver.com/post.php',options,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"Successfully dumped 1") == []);

