//<-- NO CHECK REF -->


//Simple REST PUT
result = restPUT("http://httpbin.org/put",%f,"PUT this data");
assert_checkfalse(grep(result,"PUT this data") == []);


//REST PUT, some args through the url, result stored in TMPDIR
result = restPUT("http://www.newburghschools.org/testfolder/dump.php?urlarg=urlval",%f,"PUT this data","","",TMPDIR);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"urlval") == []);
assert_checkfalse(grep(fileContents,"PUT this data") == []);


//REST PUT, some args through the url, result stored in TMPDIR
result = restPUT("http://httpbin.org/put?myarg=myvalue",%f,"PUT this data","","",TMPDIR);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"myvalue") == []);
assert_checkfalse(grep(fileContents,"PUT this data") == []);


//REST PUT, custom Headers added, result stored in TMPDIR
options = struct("argsFromFile",%f,"argsSrc","PUT this data","authUser","","authPass","","resultDest",TMPDIR);
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restPUT("http://www.newburghschools.org/testfolder/dump.php", options, headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"myheaderval") == []);
assert_checkfalse(grep(fileContents,"PUT this data") == []);


//REST PUT, custom Headers added, PUT data from file
srcFile = TMPDIR + "/restTemp";
f1 = mopen(srcFile,"w");
mputl("PUT this data",f1);
mclose(f1);
inf = fileinfo(srcFile);
assert_checkfalse(inf(1) == 0);
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restPUT("http://httpbin.org/put?myarg=myvalue",%t,srcFile,"","",TMPDIR,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"myvalue") == []);
assert_checkfalse(grep(fileContents,"PUT this data") == []);
assert_checkfalse(grep(fileContents,"myheaderval") == []);


//REST PUT, using Options struct, custom Headers and PUT data from file
srcFile = TMPDIR + "/restTemp";
f1 = mopen(srcFile,"w");
mputl("PUT this data",f1);
mclose(f1);
inf = fileinfo(srcFile);
assert_checkfalse(inf(1) == 0);
options = struct("argsFromFile",%t,"argsSrc",srcFile,"authUser","","authPass","","resultDest",TMPDIR);
headers = struct("myheader","myheaderval","myheader2","myheaderval2");
result = restPUT("http://httpbin.org/put",options,headers);
inf = fileinfo(result);
assert_checkfalse(inf(1) == 0);
f1 = mopen(result,"r");
fileContents = mgetl(f1);
mclose(f1);
assert_checkfalse(grep(fileContents,"PUT this data") == []);
assert_checkfalse(grep(fileContents,"myheaderval2") == []);
