/*
* Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
* Copyright (C) 2011 - DIGITEO - Allan CORNET
* 
* This file must be used under the terms of the CeCILL.
* This source file is licensed as described in the file COPYING, which
* you should have received as part of this distribution.  The terms
* are also available at    
* http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
*
*/
/*--------------------------------------------------------------------------*/ 
#include <stdio.h> 
#include <windows.h> 
/*--------------------------------------------------------------------------*/ 
#ifdef _WIN64
#pragma comment(lib,"../../thirdparty/curl/win64/libcurl_imp.lib")
#pragma comment(lib,"../../thirdparty/libxml2/win64/libxml2.lib")
#else
#pragma comment(lib,"../../thirdparty/curl/win32/libcurl_imp.lib")
#pragma comment(lib,"../../thirdparty/libxml2/win32/libxml2.lib")
#endif
/*--------------------------------------------------------------------------*/ 
int WINAPI DllMain (HINSTANCE hInstance , DWORD reason, PVOID pvReserved)
{
	switch (reason) 
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}
	return 1;
}
/*--------------------------------------------------------------------------*/ 

