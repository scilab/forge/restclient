/*
 * Copyright (C) 2012 - Rohan Kulkarni 
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <stdio.h>
#include "api_scilab.h"
#include "api_list.h"
#include "string.h"
#include "MALLOC.h"
#include "BOOL.h"
#include "Scierror.h"
#include "restManager.h"
#include "localization.h"
#include "restCALL.h"

/**
 * Free the memory allocated to the pointers
 */
void freeAllocatedStrings(char* method, char *url, char* argsSrc, char* authUser, char* authPass, char* resultDest, char** headers, int headerCount)
{
    int i;
    if(method != NULL)
	FREE(method);
    if(url != NULL)
	freeAllocatedSingleString(url);
    if(argsSrc != NULL)
	freeAllocatedSingleString(argsSrc);
    if(authUser != NULL)
	freeAllocatedSingleString(authUser);
    if(authPass != NULL)
	freeAllocatedSingleString(authPass);
    if(resultDest != NULL)
	freeAllocatedSingleString(resultDest);
    if(headers != NULL)
    {
	for(i = 0; i<headerCount; i++)
	    if(headers[i] != NULL)FREE(headers[i]);
	FREE(headers);
    }
}
/* ==================================================================== */

char* restCall(void *pvApiCtx, char* method, int* piAddressVarOne, int* piAddressVarTwo, int* piAddressVarThree, int* piAddressVarFour, int* piAddressVarFive, int* piAddressVarSix, int* piAddressVarSeven, char *fname)
{
    /* Variable declaration */
    SciErr sciErr;

    char *url = NULL;

    BOOL tpBoolVar = FALSE;
    BOOL *argsFromFile = &tpBoolVar;

    char *argsSrc = NULL;
    char *authUser = NULL;
    char *authPass = NULL;
    char *resultDest = NULL;
    char **headers = NULL;
    int headerCount = 0;

    int optionCount = 0;

    int i = 0;
    int ret = 0;

    int varTwoType;
    int optionsListItemCount;

    int *itemAddress = NULL;
    int itemType;

    int iRows, iCols;
    int *iLen;
    char **listItem;

    int *dims = NULL;

    int *piAddressVarHeader = NULL;

    int varHeaderType;
    int headerListItemCount;

    char *headerValue = NULL;
    char *retValue = NULL;

    /* Specify the url */
    if (!isStringType(pvApiCtx, piAddressVarOne))
    {
	Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 1);
	return 0;
    }

    ret = getAllocatedSingleString(pvApiCtx, piAddressVarOne, &url);
    if (ret)
    {
	Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 1);
	freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
	return 0;
    }

    if(Rhs > 1)
    {
	/* Check second argument type*/
	sciErr = getVarType(pvApiCtx,piAddressVarTwo,&varTwoType);
	if(sciErr.iErr)
	{
	    printError(&sciErr,0);
	    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
	    return 0;
	}

	if(varTwoType == sci_boolean)
	{
	    ret = getScalarBoolean(pvApiCtx, piAddressVarTwo, argsFromFile);
	    if (ret)
	    {
		Scierror(999, _("%s: Wrong type for argument %d: A scalar boolean.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    if(Rhs > 2)
	    {
		/* Specify argsSrc */
		if (!isStringType(pvApiCtx, piAddressVarThree))
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 3);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		ret = getAllocatedSingleString(pvApiCtx, piAddressVarThree, &argsSrc);
		if (ret)
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 3);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

	    }
	    if(Rhs > 3)
	    {
		/* Specify authUser */
		if (!isStringType(pvApiCtx, piAddressVarFour))
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 4);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		ret = getAllocatedSingleString(pvApiCtx, piAddressVarFour, &authUser);
		if (ret)
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 4);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

	    }
	    if(Rhs > 4)
	    {
		/* Specify authPass */
		if (!isStringType(pvApiCtx, piAddressVarFive))
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 5);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		ret = getAllocatedSingleString(pvApiCtx, piAddressVarFive, &authPass);
		if (ret)
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 5);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

	    }
	    if(Rhs > 5)
	    {
		/* Specify resultDest */
		if (!isStringType(pvApiCtx, piAddressVarSix))
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 6);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		ret = getAllocatedSingleString(pvApiCtx, piAddressVarSix, &resultDest);
		if (ret)
		{
		    Scierror(999, _("%s: Wrong type for argument %d: A single string.\n"), fname, 6);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

	    }
	}
	else if(varTwoType == sci_mlist)
	{
	    /* Specify Options Struct */
	    sciErr = getListItemNumber(pvApiCtx, piAddressVarTwo, &optionsListItemCount);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;    
	    }

	    optionCount = optionsListItemCount - 2;

	    if(optionCount != 5)
	    {
		Scierror(999, _("%s: Wrong number of fields in struct at %d: Five fields.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    itemAddress = NULL;

	    sciErr = getListItemAddress(pvApiCtx, piAddressVarTwo, 1 , &itemAddress);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
	    if(itemType != sci_strings)
	    {
		Scierror(999, _("%s: Wrong type of argument %d: A  struct type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    /* Get Options Fields */
	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 1, &iRows, &iCols, NULL, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    /* Check iRows = 1 and iCols = 7 */
	    if((iRows) != 1 || (iCols) != 7)
	    {
		Scierror(999, _("%s: Wrong number of fields in struct at %d: Five fields.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    iLen = (int*)MALLOC(sizeof(int) * (iRows) * (iCols));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 1, &iRows, &iCols, iLen, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    listItem = (char **)MALLOC(sizeof(char*) * (iRows) * (iCols));

	    for(i = 0; i< (iRows) * (iCols); i++)
	    {
		listItem[i] = (char *)MALLOC(sizeof(char) * (iLen[i] + 1));
	    }

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 1, &iRows, &iCols, iLen, listItem);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}

		for(i = 0; i<7; i++)
		    if(listItem[i] != NULL)
			FREE(listItem[i]);
		FREE(listItem);
		return 0;
	    }



	    /* Check the fields in listItem */
	    if((strcmp(listItem[0],"st")!=0) ||
		    (strcmp(listItem[1],"dims") != 0) ||
		    (strcmp(listItem[2],"argsFromFile") != 0) ||
		    (strcmp(listItem[3],"argsSrc") != 0) ||
		    (strcmp(listItem[4],"authUser") != 0) ||
		    (strcmp(listItem[5],"authPass") != 0) ||
		    (strcmp(listItem[6],"resultDest") != 0))
	    {
		Scierror(999, _("%s: Wrong struct field values for argument %d. Refer help page.\n"),fname, 2);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}

		for(i = 0; i<7; i++)
		    if(listItem[i] != NULL)
			FREE(listItem[i]);
		FREE(listItem);
		return 0;
	    }

	    /* Free memory */
	    if(iLen != NULL){FREE(iLen);iLen = NULL;}

	    for(i = 0; i<7; i++)
		if(listItem[i] != NULL)
		    FREE(listItem[i]);
	    FREE(listItem);


	    /* Check dims item of struct, must be 1. 1. */
	    sciErr = getListItemAddress(pvApiCtx, piAddressVarTwo, 2 , &itemAddress);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
	    if(itemType != sci_ints)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 2 in argument %d: A integer matrix type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    dims = NULL;

	    sciErr = getMatrixOfInteger32InList(pvApiCtx, piAddressVarTwo, 2, &iRows, &iCols, &dims);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    if((iRows) != 1 || (iCols) != 2)
	    {
		Scierror(999, _("%s: Wrong type of struct in argument %d: Size of item dims = 1. 2.\n"), fname, 7);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    if(dims[0] != 1 || dims[1] != 1)
	    {
		Scierror(999, _("%s: Wrong type of struct in argument %d: Item dims = 1. 1.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }


	    /* get argsFromFile */
	    sciErr = getListItemAddress(pvApiCtx, piAddressVarTwo, 3, &itemAddress);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
	    if(itemType != sci_boolean)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 1 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getMatrixOfBooleanInList(pvApiCtx, piAddressVarTwo, 3, &iRows, &iCols, &argsFromFile);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    /* Check iRows = 1 iCols = 1 */
	    if((iRows) != 1 || (iCols) != 1)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 1 in argument %d: A boolean type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    /*  sciErr = getMatrixOfBooleanInList(pvApiCtx, piAddressVarTwo, 3, &iRows, &iCols, &argsFromFile);
		if (sciErr.iErr)
		{
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
		}
	     */


	    /* get argsSrc */
	    sciErr = getListItemAddress(pvApiCtx, piAddressVarTwo, 4 , &itemAddress);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
	    if(itemType != sci_strings)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 2 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    iLen = (int*)MALLOC(sizeof(int));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 4, &iRows, &iCols, NULL, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    /* Check iRows = 1 iCols = 1 */
	    if((iRows) != 1 || (iCols) != 1)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 2 in argument %d: A string type.\n"), fname, 2);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 4, &iRows, &iCols, iLen, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    argsSrc = (char *)MALLOC(sizeof(char)*(*iLen + 1));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 4, &iRows, &iCols, iLen, &argsSrc);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }


	    /* Free memory */
	    if(iLen != NULL){FREE(iLen);iLen = NULL;}

	    /* get authUser */
	    sciErr = getListItemAddress(pvApiCtx, piAddressVarTwo, 5 , &itemAddress);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
	    if(itemType != sci_strings)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 3 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 5, &iRows, &iCols, NULL, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    /* Check iRows = 1 iCols = 1 */
	    if((iRows) != 1 || (iCols) != 1)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 3 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    iLen = (int*)MALLOC(sizeof(int));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 5, &iRows, &iCols, iLen, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    authUser = (char *)MALLOC(sizeof(char)*(*iLen + 1));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 5, &iRows, &iCols, iLen, &authUser);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    /* Free memory */
	    if(iLen != NULL){FREE(iLen);iLen = NULL;}

	    /* get authPass */
	    sciErr = getListItemAddress(pvApiCtx, piAddressVarTwo, 6 , &itemAddress);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
	    if(itemType != sci_strings)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 4 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 6, &iRows, &iCols, NULL, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    /* Check iRows = 1 iCols = 1 */
	    if((iRows) != 1 || (iCols) != 1)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position4 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    iLen = (int*)MALLOC(sizeof(int));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 6, &iRows, &iCols, iLen, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    authPass = (char *)MALLOC(sizeof(char)*(*iLen + 1));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 6, &iRows, &iCols, iLen, &authPass);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }


	    /* Free memory */
	    if(iLen != NULL){FREE(iLen);iLen = NULL;}


	    /* get resultDest */
	    sciErr = getListItemAddress(pvApiCtx, piAddressVarTwo, 7 , &itemAddress);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
	    if(itemType != sci_strings)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 5 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 7, &iRows, &iCols, NULL, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    /* Check iRows = 1 iCols = 1 */
	    if((iRows) != 1 || (iCols) != 1)
	    {
		Scierror(999, _("%s: Wrong type for struct field value at position 5 in argument %d: A string type.\n"), fname, 2);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    iLen = (int*)MALLOC(sizeof(int));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 7, &iRows, &iCols, iLen, NULL);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    resultDest = (char *)MALLOC(sizeof(char)*(*iLen + 1));

	    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarTwo, 7, &iRows, &iCols, iLen, &resultDest);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		/* Free memory */
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		return 0;
	    }

	    /* Free memory */
	    if(iLen != NULL){FREE(iLen);iLen = NULL;}

	}
	else
	{
	    Scierror(999, _("%s: Wrong type for argument %d: A struct or boolean.\n"),fname,2);
	    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
	    return 0;
	}

	/* Specify headers */

	if(varTwoType == sci_mlist && Rhs > 2)
	{
	    piAddressVarHeader = piAddressVarThree;
	}
	else if(varTwoType == sci_boolean && Rhs > 6)
	{
	    piAddressVarHeader = piAddressVarSeven;
	}

	if(piAddressVarHeader != NULL)
	{
	    sciErr = getVarType(pvApiCtx, piAddressVarHeader, &varHeaderType);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    if(varHeaderType != sci_mlist)
	    {
		Scierror(999, _("%s: Wrong type for argument %d: A struct.\n"), fname, 7);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;
	    }

	    sciErr = getListItemNumber(pvApiCtx,piAddressVarHeader,&headerListItemCount);
	    if (sciErr.iErr)
	    {
		printError(&sciErr, 0);
		freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		return 0;    
	    }

	    headerCount = headerListItemCount - 2;
	    if(headerCount != 0)
	    {

		itemAddress = NULL;
		sciErr = getListItemAddress(pvApiCtx, piAddressVarHeader, 1 , &itemAddress);
		if (sciErr.iErr)
		{
		    printError(&sciErr, 0);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
		if(itemType != sci_strings)
		{
		    Scierror(999, _("%s: Wrong type for struct list in argument %d: A struct type.\n"), fname, 7);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		/* Get Headers Matrix */
		sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarHeader, 1, &iRows, &iCols, NULL, NULL);
		if (sciErr.iErr)
		{
		    printError(&sciErr, 0);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		/* iRows == 1 for first item */
		if((iRows) != 1)
		{
		    Scierror(999, _("%s: Wrong type for struct list in argument %d: A struct type.\n"), fname, 7);
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    return 0;
		}

		iLen = (int*)MALLOC(sizeof(int) * (iRows) * (iCols));

		sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarHeader, 1, &iRows, &iCols, iLen, NULL);
		if (sciErr.iErr)
		{
		    printError(&sciErr, 0);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    if(iLen != NULL){FREE(iLen);iLen = NULL;}
		    return 0;
		}

		listItem = (char **)MALLOC(sizeof(char*) * (iRows) * (iCols)); 

		for(i = 0; i< (iRows) * (iCols); i++)
		{
		    listItem[i] = (char*)MALLOC(sizeof(char)*(iLen[i] + 1));
		}


		sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarHeader, 1, &iRows, &iCols, iLen, listItem);
		if (sciErr.iErr)
		{
		    printError(&sciErr, 0);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    if(iLen != NULL){FREE(iLen);iLen = NULL;}

		    for(i = 0; i<headerListItemCount; i++)
			if(listItem[i] != NULL)
			    FREE(listItem[i]);
		    FREE(listItem);
		    return 0;
		}

		//free ilen
		if(iLen != NULL){FREE(iLen);iLen = NULL;}

		/* Check First two values of ItemOne to be "st" and "dim" */
		if((strcmp(listItem[0], "st") != 0) ||
			(strcmp(listItem[1], "dims") != 0))
		{
		    Scierror(999, _("%s: Wrong struct field values for argument %d. Refer help page.\n"),fname, 7);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);

		    for(i = 0; i<headerListItemCount; i++)
			if(listItem[i] != NULL)
			    FREE(listItem[i]);
		    FREE(listItem);
		    return 0;
		}

		/* Check dims item of struct, must be 1. 1. */
		sciErr = getListItemAddress(pvApiCtx, piAddressVarHeader, 2 , &itemAddress);
		if (sciErr.iErr)
		{
		    printError(&sciErr, 0);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);

		    for(i = 0; i<headerListItemCount; i++)
			if(listItem[i] != NULL)
			    FREE(listItem[i]);
		    FREE(listItem);
		    return 0;
		}

		sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
		if(itemType != sci_ints)
		{
		    Scierror(999, _("%s: Wrong type for struct field value at position 2 in argument %d: A integer matrix type.\n"), fname, 7);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);

		    for(i = 0; i<headerListItemCount; i++)
			if(listItem[i] != NULL)
			    FREE(listItem[i]);
		    FREE(listItem);
		    return 0;
		}

		dims = NULL;

		sciErr = getMatrixOfInteger32InList(pvApiCtx, piAddressVarHeader, 2, &iRows, &iCols, &dims);
		if (sciErr.iErr)
		{
		    printError(&sciErr, 0);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);

		    for(i = 0; i<headerListItemCount; i++)
			if(listItem[i] != NULL)
			    FREE(listItem[i]);
		    FREE(listItem);

		    return 0;
		}

		if((iRows) != 1 || (iCols) != 2)
		{
		    Scierror(999, _("%s: Wrong type of struct in argument %d: Size of item dims = 1. 2.\n"), fname, 7);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    if(iLen != NULL){FREE(iLen);iLen = NULL;}

		    for(i = 0; i<headerListItemCount; i++)
			if(listItem[i] != NULL)
			    FREE(listItem[i]);
		    FREE(listItem);

		    return 0;
		}

		if(dims[0] != 1 || dims[1] != 1)
		{
		    Scierror(999, _("%s: Wrong type of struct in argument %d: Item dims = 1. 1.\n"), fname, 7);
		    /* Free memory */
		    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);
		    if(iLen != NULL){FREE(iLen);iLen = NULL;}

		    for(i = 0; i<headerListItemCount; i++)
			if(listItem[i] != NULL)
			    FREE(listItem[i]);
		    FREE(listItem);

		    return 0;
		}


		headers = (char **)MALLOC(sizeof(char*)*headerCount);

		/* Iterate list to retrive headers and values */
		for(i = 2; i< headerListItemCount ; i++)
		{

		    sciErr = getListItemAddress(pvApiCtx, piAddressVarHeader, i + 1 , &itemAddress);
		    if (sciErr.iErr)
		    {
			printError(&sciErr, 0);
			/* Free memory */
			freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, i - 2);
			if(iLen != NULL){FREE(iLen);iLen = NULL;}

			for(i = 0; i<headerListItemCount; i++)
			    if(listItem[i] != NULL)
				FREE(listItem[i]);
			FREE(listItem);
			return 0;
		    }

		    sciErr = getVarType(pvApiCtx,itemAddress,&itemType);
		    if(itemType != sci_strings)
		    {
			Scierror(999, _("%s: Wrong type for struct field value at position %d in argument %d: A string type.\n"), fname, i - 1, 7);
			/* Free memory */
			freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, i - 2);
			if(iLen != NULL){FREE(iLen);iLen = NULL;}

			for(i = 0; i<headerListItemCount; i++)
			    if(listItem[i] != NULL)
				FREE(listItem[i]);
			FREE(listItem);
			return 0;
		    }

		    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarHeader, i + 1, &iRows, &iCols, NULL, NULL);
		    if (sciErr.iErr)
		    {
			printError(&sciErr, 0);
			/* Free memory */
			freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, i - 2);
			if(iLen != NULL){FREE(iLen);iLen = NULL;}

			for(i = 0; i<headerListItemCount; i++)
			    if(listItem[i] != NULL)
				FREE(listItem[i]);
			FREE(listItem);
			return 0;
		    }

		    /* Check iRows = 1 iCols = 1 */
		    if((iRows) != 1 || (iCols) != 1)
		    {
			Scierror(999, _("%s: Wrong type for struct field value at position %d in argument %d: A string type.\n"), fname, i - 1, 7);
			/* Free memory */
			freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, i - 2);
			if(iLen != NULL){FREE(iLen);iLen = NULL;}

			for(i = 0; i<headerListItemCount; i++)
			    if(listItem[i] != NULL)
				FREE(listItem[i]);
			FREE(listItem);
			return 0;
		    }

		    /* Free memory */
		    if(iLen != NULL){FREE(iLen);iLen = NULL;}

		    iLen = (int*)MALLOC(sizeof(int));

		    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarHeader, i + 1, &iRows, &iCols, iLen, NULL);
		    if (sciErr.iErr)
		    {
			printError(&sciErr, 0);
			/* Free memory */
			freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, i - 2);
			if(iLen != NULL){FREE(iLen);iLen = NULL;}

			for(i = 0; i<headerListItemCount; i++)
			    if(listItem[i] != NULL)
				FREE(listItem[i]);
			FREE(listItem);
			return 0;
		    }

		    headerValue = (char *)MALLOC(sizeof(char) * ((*iLen) + 1));

		    sciErr = getMatrixOfStringInList(pvApiCtx, piAddressVarHeader, i + 1, &iRows, &iCols, iLen, &headerValue);
		    if (sciErr.iErr)
		    {
			printError(&sciErr, 0);
			/* Free memory */
			freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, i - 2);
			FREE(headerValue);
			if(iLen != NULL){FREE(iLen);iLen = NULL;}

			for(i = 0; i<headerListItemCount; i++)
			    if(listItem[i] != NULL)
				FREE(listItem[i]);
			FREE(listItem);
			return 0;
		    }

		    headers[i-2] = (char *)MALLOC(sizeof(char)*((*iLen) + strlen(listItem[i]) + 2));

		    strcpy(headers[i-2],listItem[i]);
		    strcat(headers[i-2],":");
		    strcat(headers[i-2], headerValue);

		    FREE(headerValue);

		}

		/* Free memory */
		if(iLen != NULL){FREE(iLen);iLen = NULL;}
		for(i = 0; i<headerListItemCount; i++)
		    if(listItem[i] != NULL)
			FREE(listItem[i]);
		FREE(listItem);

	    }
	}
    }

    /* Set to NULL if 0 length strings */
    if(argsSrc != NULL)
	if(strlen(argsSrc) == 0){FREE(argsSrc);argsSrc = NULL;}
    if(authUser != NULL)
	if(strlen(authUser) == 0){FREE(authUser);authUser = NULL;}
    if(authPass != NULL)
	if(strlen(authPass) == 0){FREE(authPass);authPass = NULL;}
    if(resultDest != NULL)
	if(strlen(resultDest) == 0){FREE(resultDest);resultDest = NULL;}

    /* call function  */

    retValue = restPerform(method, url, *argsFromFile, argsSrc, authUser, authPass, resultDest, headers, headerCount);
    
    /* Free memory */
    freeAllocatedStrings(method, url, argsSrc, authUser, authPass, resultDest, headers, headerCount);

    return retValue;

}
